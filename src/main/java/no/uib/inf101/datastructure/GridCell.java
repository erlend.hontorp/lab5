package no.uib.inf101.datastructure;

// Les om records her: https://inf101.ii.uib.no/notat/mutabilitet/#record

// import java.awt.Color;

/**
 * A GridCell contains a CellPosition and a T.
 *
 * @param pos the position (CellPosition)of the cell
 * @param elem ...
 */
public record GridCell<T>(CellPosition pos, T elem) {}
